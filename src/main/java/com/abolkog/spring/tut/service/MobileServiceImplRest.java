package com.abolkog.spring.tut.service;

import com.abolkog.spring.tut.model.Mobile;
import com.abolkog.spring.tut.model.Receipt;
import com.abolkog.spring.tut.model.TransactionType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "com.abolkog.spring.tut.service.MobileService")
public class MobileServiceImplRest implements MobileServiceRest {

    private static final String FILE_PATH = "mobiles.json";
    @Override
    public List<Mobile> getAllMobiles()  {

        List<Mobile> mobiles = new ArrayList<>();
        Mobile mobile=new Mobile();
        mobile.setBrand("kk");
        mobile.setBranchCode("11");
        mobile.setId("1");
        mobile.setPrice(100);
        mobiles.add(mobile);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(FILE_PATH), mobiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File file = new File(FILE_PATH);
            if (file.exists()) {
                ObjectMapper mapper = new ObjectMapper();
                mobiles = Arrays.asList(mapper.readValue(file, Mobile[].class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mobiles;
    }

    @Override
    public List<Mobile> searchMobiles(String brand, String model, String branchCode) throws IOException {
        List<Mobile> mobiles = new ArrayList<>();
        try {
            File file = new File(FILE_PATH);
            if (file.exists()) {
                ObjectMapper mapper = new ObjectMapper();
                mobiles = Arrays.asList(mapper.readValue(file, Mobile[].class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mobiles.stream()
                .filter(mobile -> (brand == null || mobile.getBrand().equalsIgnoreCase(brand)))
                .filter(mobile -> (model == null || mobile.getModel().equalsIgnoreCase(model)))
                .filter(mobile -> (branchCode == null || mobile.getBranchCode().equalsIgnoreCase(branchCode)))
                .collect(Collectors.toList());
    }

    public Mobile addMobile(Mobile mobile) {
        try {
            // read the existing mobiles from file
            List<Mobile> mobiles = getAllMobiles();

            // set a new ID for the mobile
            mobile.setId(String.valueOf(mobiles.size() + 1));

            // add the mobile to the list
            mobiles.add(mobile);

            // write the updated mobiles list to file
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(FILE_PATH), mobiles);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mobile;
    }
    public boolean deleteMobile(String id) {
        try {
            // read the existing mobiles from file
            List<Mobile> mobiles = getAllMobiles();

            // find the mobile with the given ID
            Mobile mobileToDelete = mobiles.stream()
                    .filter(mobile -> mobile.getId().equals(id))
                    .findFirst()
                    .orElse(null);

            // if mobile found, remove it from the list and write updated list to file
            if (mobileToDelete != null) {
                mobiles.remove(mobileToDelete);
                ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(new File(FILE_PATH), mobiles);
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
