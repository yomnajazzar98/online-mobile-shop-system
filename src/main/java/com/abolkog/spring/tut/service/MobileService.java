package com.abolkog.spring.tut.service;

import com.abolkog.spring.tut.model.Mobile;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface MobileService {

    @WebMethod
    void buyMobile(String customerName, String mobileNumber, String mobileModel, String mobilePrice);

    @WebMethod
    void retrieveMobilePrize(String customerName, String mobileNumber, String mobileModel, String mobilePrice);
}

