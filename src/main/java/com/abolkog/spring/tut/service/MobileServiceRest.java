package com.abolkog.spring.tut.service;

import com.abolkog.spring.tut.model.Mobile;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface MobileServiceRest {

    @WebMethod
    List<Mobile> getAllMobiles();

    @WebMethod
    List<Mobile> searchMobiles(String brand, String model, String branchCode) throws IOException;
    @WebMethod
     Mobile addMobile(Mobile mobile);
    @WebMethod
    boolean deleteMobile(String id);
}

