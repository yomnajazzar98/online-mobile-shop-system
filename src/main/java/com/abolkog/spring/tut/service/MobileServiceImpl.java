package com.abolkog.spring.tut.service;

import com.abolkog.spring.tut.model.Mobile;
import com.abolkog.spring.tut.model.Receipt;
import com.abolkog.spring.tut.model.TransactionType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.internal.org.objectweb.asm.TypeReference;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "com.abolkog.spring.tut.service.MobileService")
public class MobileServiceImpl implements MobileService {

    private static final String FILE_PATH = "receipts.json";
    private List<Receipt> receipts;

    public MobileServiceImpl() {
        receipts = new ArrayList<>();
        readReceiptsFromFile();
    }

    private void readReceiptsFromFile() {
        try {
            File file = new File(FILE_PATH);
            if (file.exists()) {
                ObjectMapper mapper = new ObjectMapper();
                receipts = Arrays.asList(mapper.readValue(file, Receipt[].class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void writeReceiptsToFile() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(FILE_PATH), receipts);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void buyMobile(String customerName, String mobileNumber, String mobileModel, String mobilePrice) {

        // Create a new receipt object
        Receipt receipt = new Receipt();
        receipt.setCustomerName(customerName);
        receipt.setMobileNumber(mobileNumber);
        receipt.setMobileModel(mobileModel);
        receipt.setMobilePrice(mobilePrice);
        receipt.setTransactionType(TransactionType.BUY);

        // Add the receipt to the list and write it to the file
        receipts.add(receipt);
        writeReceiptsToFile();

        // Print the receipt to the console
        System.out.println("Receipt for buying mobile:");
        System.out.println("-------------------------");
        System.out.println("Customer name: " + receipt.getCustomerName());
        System.out.println("Mobile number: " + receipt.getMobileNumber());
        System.out.println("Mobile model: " + receipt.getMobileModel());
        System.out.println("Mobile price: " + receipt.getMobilePrice());
        System.out.println("-------------------------");
    }

    @Override
    public void retrieveMobilePrize(String customerName, String mobileNumber, String mobileModel, String mobilePrice) {
        // Create a new receipt object
        Receipt receipt = new Receipt();
        receipt.setCustomerName(customerName);
        receipt.setMobileNumber(mobileNumber);
        receipt.setMobileModel(mobileModel);
        receipt.setMobilePrice(mobilePrice);
        receipt.setTransactionType(TransactionType.RETRIEVE);

        // Add the receipt to the list and write it to the file
        receipts.add(receipt);
        writeReceiptsToFile();

        // Print the receipt to the console
        System.out.println("Receipt for retrieving mobile prize:");
        System.out.println("-------------------------");
        System.out.println("Customer name: " + receipt.getCustomerName());
        System.out.println("Mobile number: " + receipt.getMobileNumber());
        System.out.println("Mobile model: " + receipt.getMobileModel());
        System.out.println("Mobile price: " + receipt.getMobilePrice());
        System.out.println("-------------------------");
    }





}
