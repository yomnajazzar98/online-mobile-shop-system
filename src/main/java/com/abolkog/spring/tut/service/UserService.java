package com.abolkog.spring.tut.service;

import com.abolkog.spring.tut.model.User;

public interface UserService {
    User authenticateUser(String email, String password);
}
