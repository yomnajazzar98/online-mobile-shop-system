package com.abolkog.spring.tut.service;

import com.abolkog.spring.tut.model.User;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private List<User> users = Arrays.asList(
            new User("john@example.com", "John Doe", "user", "password"),
            new User("jane@example.com", "Jane Doe", "admin", "password")
    );

    @Override
    public User authenticateUser(String email, String password) {
        // find the user with the given email
        User user = users.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst()
                .orElse(null);

        // check if the user exists and has the correct password
        if (user != null && user.getPassword().equals(password)) {
            return user;
        } else {
            return null;
        }
    }
}
