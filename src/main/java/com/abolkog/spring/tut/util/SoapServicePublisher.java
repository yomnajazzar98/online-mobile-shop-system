package com.abolkog.spring.tut.util;

import com.abolkog.spring.tut.service.MobileServiceImpl;

import javax.xml.ws.Endpoint;

public class SoapServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8082/ws/soap-service",new MobileServiceImpl());
    }
}
