package com.abolkog.spring.tut.util;
import com.abolkog.spring.tut.service.MobileServiceImpl;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import com.abolkog.spring.tut.service.MobileService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class SoapClient extends Application{
    private static MobileService mobileService=new MobileServiceImpl();

    public static void main(String[] args) throws MalformedURLException {
        try {

            URL url = new URL("http://localhost:8082/ws/soap-service?wsdl");

            QName qname = new QName("http://service.tut.spring.abolkog.com/", "MobileServiceImplService");
            Service service = Service.create(url, qname);
             mobileService = service.getPort(MobileService.class);
            launch(args);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Mobile Shop");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);

        grid.setPadding(new Insets(25, 25, 25, 25));
        Label header = new Label("Create Invoice :");
        header.setFont(Font.font("Arial", 24));
        grid.add(header, 0, 0);


        Label mobileNumberLabel = new Label("Mobile Number:");
        mobileNumberLabel.setFont(Font.font("Arial", 14));
        grid.add(mobileNumberLabel, 0, 1);
        TextField mobileNumberTextField = new TextField();
        mobileNumberTextField.setStyle("-fx-font-size: 14px;");
        grid.add(mobileNumberTextField, 1, 1);

        Label mobileModelLabel = new Label("Mobile Model:");
        mobileModelLabel.setFont(Font.font("Arial", 14));
        grid.add(mobileModelLabel, 0, 2);
        TextField mobileModelTextField = new TextField();
        mobileModelTextField.setStyle("-fx-font-size: 14px;");
        grid.add(mobileModelTextField, 1, 2);

        Label customerNameLabel = new Label("Customer Name:");
        customerNameLabel.setFont(Font.font("Arial", 14));
        grid.add(customerNameLabel, 0, 3);
        TextField customerNameTextField = new TextField();
        customerNameTextField.setStyle("-fx-font-size: 14px;");
        grid.add(customerNameTextField, 1, 3);

        Label mobilePriceLabel = new Label("Mobile Price:");
        mobilePriceLabel.setFont(Font.font("Arial", 14));
        grid.add(mobilePriceLabel, 0, 4);
        TextField mobilePriceTextField = new TextField();
        mobilePriceTextField.setStyle("-fx-font-size: 14px;");
        grid.add(mobilePriceTextField, 1, 4);

        Label transactionTypeLabel = new Label("Transaction Type:");
        transactionTypeLabel.setFont(Font.font("Arial", 14));
        grid.add(transactionTypeLabel, 0, 5);
        ComboBox<String> transactionTypeComboBox = new ComboBox<>();
        transactionTypeComboBox.setStyle("-fx-font-size: 14px;");
        transactionTypeComboBox.getItems().addAll("Buy", "Retrieve");
        grid.add(transactionTypeComboBox, 1, 5);

        Button submitButton = new Button("Submit");
        submitButton.setStyle("-fx-font-size: 14px; -fx-background-color: #4CAF50; -fx-text-fill: white;");
        grid.add(submitButton, 1, 6);
        Label statusLabel = new Label();
        statusLabel.setFont(Font.font("Arial", 14));
        grid.add(statusLabel, 1, 7);

        submitButton.setOnAction(event -> {
            String mobileNumber = mobileNumberTextField.getText();
            String mobileModel = mobileModelTextField.getText();
            String transactionType = transactionTypeComboBox.getValue();
            String customerName = customerNameTextField.getText();
            String mobilePrice = mobilePriceTextField.getText();
            try {
                if ("Buy".equals(transactionType)) {
                    mobileService.buyMobile(customerName, mobileNumber, mobileModel, mobilePrice);
                } else if ("Retrieve".equals(transactionType)) {
                    mobileService.retrieveMobilePrize(customerName, mobileNumber, mobileModel, mobilePrice);
                }
                statusLabel.setText("Transaction successful");
                mobileNumberTextField.clear();
                mobileModelTextField.clear();
                customerNameTextField.clear();
                mobilePriceTextField.clear();
                transactionTypeComboBox.getSelectionModel().clearSelection();
            } catch (Exception e) {
                statusLabel.setText(e.getMessage());
            }
        });

        Scene scene = new Scene(grid, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}