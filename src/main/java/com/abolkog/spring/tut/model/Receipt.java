package com.abolkog.spring.tut.model;

import java.time.LocalDateTime;

public class Receipt {
    private String customerName;
    private String mobileNumber;
    private String mobileModel;
    private String mobilePrice;
    private TransactionType transactionType;
    private LocalDateTime dateTime;
    private String receiptNumber;

    public Receipt() {
    }

    // Constructors, getters, and setters

    // toString method to print the contents of the receipt
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Receipt for ");
        sb.append(transactionType == TransactionType.BUY ? "buying" : "retrieving");
        sb.append(" mobile:\n");
        sb.append("-------------------------\n");
        sb.append("Customer name: ").append(customerName).append("\n");
        sb.append("Mobile number: ").append(mobileNumber).append("\n");
        sb.append("Mobile model: ").append(mobileModel).append("\n");
        sb.append("Mobile price: $").append(String.format("%.2f", mobilePrice)).append("\n");
        sb.append("Transaction type: ").append(transactionType).append("\n");
        sb.append("Date and time: ").append(dateTime).append("\n");
        sb.append("Receipt number: ").append(receiptNumber).append("\n");
        sb.append("-------------------------\n");
        return sb.toString();
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setMobileModel(String mobileModel) {
        this.mobileModel = mobileModel;
    }

    public void setMobilePrice(String mobilePrice) {
        this.mobilePrice = mobilePrice;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getMobilePrice() {
        return mobilePrice;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getMobileModel() {
        return mobileModel;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }
}