package com.abolkog.spring.tut.model;

public enum TransactionType {
    BUY("Buy"),
    RETRIEVE("Retrieve");

    private final String name;

    TransactionType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}