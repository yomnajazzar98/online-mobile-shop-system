package com.abolkog.spring.tut.controller;

import com.abolkog.spring.tut.model.LoginRequest;
import com.abolkog.spring.tut.model.Mobile;
import com.abolkog.spring.tut.model.User;
import com.abolkog.spring.tut.service.MobileServiceRest;
import com.abolkog.spring.tut.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/mobiles")
public class MobileController {

    @Autowired
    private MobileServiceRest mobileServicerest;
    @Autowired
    private UserService userService;
    @GetMapping
    public List<Mobile> getAllMobiles() throws IOException {
        return mobileServicerest.getAllMobiles();
    }
    @RequestMapping("/welcome")
    public ModelAndView welcome() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index.html");
        return modelAndView;
    }
    @GetMapping("/search")
    public List<Mobile> searchMobiles(@RequestParam(required = false) String brand,
                                      @RequestParam(required = false) String model,
                                      @RequestParam(required = false) String branchCode) throws IOException {
        return mobileServicerest.searchMobiles(brand, model, branchCode);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody LoginRequest loginRequest) {
        // validate the input data
        if (loginRequest.getEmail() == null || loginRequest.getEmail().trim().isEmpty()) {
            throw new IllegalArgumentException("Email is required.");
        }
        if (loginRequest.getPassword() == null || loginRequest.getPassword().trim().isEmpty()) {
            throw new IllegalArgumentException("Password is required.");
        }

        // check if the user exists in the system
        User user = userService.authenticateUser(loginRequest.getEmail(), loginRequest.getPassword());
        if (user != null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Login failed", HttpStatus.BAD_REQUEST);
        }



    }
}